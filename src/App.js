import Header from './components/Header';
import Search from './components/Search';
import CardList from './components/CardList';
import { useEffect, useState } from 'react';

function App() {
  const [listItems, setListItems] = useState([]);
  const [isCalling, setIsCalling] = useState(true);

  useEffect(() => {
    callToApi();
  }, []);

  const callToApi = async (text = 'marte') => {
    try {
      const respuesta = await fetch(
        `https://images-api.nasa.gov/search?q=${text}&page=1&media_type=image`
      );
      const res = await respuesta.json();
      setListItems(res.collection.items);
      setIsCalling(false);
    } catch (error) {
      console.log(error);
      setIsCalling(false);
    }
  };

  if (isCalling) return <p>Cargando .....</p>;

  return (
    <div className='App'>
      <Header title={"Academia Moviles xD"}></Header>
      <main className='.bg-light'>
        <section className='container'>
          <Search onCallToApi={callToApi}/>
          <CardList listItems={listItems} />
        </section>
      </main>
    </div>
  );
}

export default App;
